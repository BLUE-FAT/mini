// custom-tab-bar/index.js

Component({
  /**
   * 页面的初始数据
   */
  data: {
    active: 0,
    list: [
      {
        url: "/pages/main/index/index",
        icon: "wap-home",
        text: "首页"
      }
    ]
  },
  lifetimes: {
    attached: function () {
      // 在组件实例进入页面节点树时执行
    }
  },
  pageLifetimes: {
    show: function () {
      // 页面被展示
    }
  },
  methods: {
    onChange(event) {
      // event.detail 的值为当前选中项的索引
      this.setData({
        active: event.detail
      });
      wx.switchTab({
        url: event.detail
      });
    }
  },
  init() {
    const page = getCurrentPages().pop();
    this.setData({
      active: this.data.list.findIndex(item => item.url === `/${page.route}`)
    });
  }
});
