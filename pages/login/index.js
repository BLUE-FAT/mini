// pages/main/login/index.js

import {
  usernameLogin,checkHasLogined
} from "../../utils/auth"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    loading: false,
    username: "",
    password: "",
    nameError: '',
    passwordError: ''
  },

  handleLogin() {
    const {
      username,
      password
    } = this.data

    if (!username) {
      return this.setData({
        nameError: '请输入用户名'
      })
    } else {
      this.setData({
        nameError: ''
      })
    }
    if (!password) {
      return this.setData({
        passwordError: '请输入密码'
      })
    } else {
      this.setData({
        nameError: ''
      })
    }

    this.setData({
      loading: true
    })
    usernameLogin({
      username,
      password
    }).then(res => {
      wx.switchTab({
        url: '../homepage/index'
      })
    }).catch((e) => {
      console.log(e)
      this.setData({
        loading: false
      })
      wx.showToast({
        title: (e && e.message) || '登录失败',
        icon: 'none',
        duration: 2000
      })

    })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 自动登录
    checkHasLogined().then(isLogined => {
      if (isLogined) {
        wx.reLaunch({
          url: "/pages/homepage/index"
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})