//app.js
import { login, checkHasLogined } from "utils/auth";

App({
  onLaunch: function () {
    const that = this;
    const updateManager = wx.getUpdateManager();
    updateManager.onUpdateReady(function () {
      wx.showModal({
        title: "更新提示",
        content: "新版本已经准备好，是否重启应用？",
        success(res) {
          if (res.confirm) {
            updateManager.applyUpdate();
          }
        }
      });
    });

    wx.getNetworkType({
      success(res) {
        const networkType = res.networkType;
        if (networkType === "none") {
          that.globalData.isConnected = false;
          wx.showToast({
            title: "当前无网络",
            icon: "loading",
            duration: 2000
          });
        }
      }
    });

    wx.onNetworkStatusChange(function (res) {
      if (!res.isConnected) {
        that.globalData.isConnected = false;
        wx.showToast({
          title: "网络已断开",
          icon: "loading",
          duration: 2000
        });
      } else {
        that.globalData.isConnected = true;
        wx.hideToast();
      }
    });

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting["scope.userInfo"]) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo;

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res);
              }
            }
          });
        }
      }
    });
  },
  onShow(e) {
    // 自动登录
    checkHasLogined().then(isLogined => {
      if (!isLogined) {
        login();
      }
    });
  },

  // 设置监听器
  watch: function (ctx, obj) {
    Object.keys(obj).forEach(key => {
      this.observer(ctx.data, key, ctx.data[key], function (value) {
        obj[key].call(ctx, value);
      });
    });
  },
  // 监听属性，并执行监听函数
  observer: function (data, key, val, fn) {
    Object.defineProperty(data, key, {
      configurable: true,
      enumerable: true,
      get: function () {
        return val;
      },
      set: function (newVal) {
        if (newVal === val) return;
        fn && fn(newVal);
        val = newVal;
      }
    });
  },
  globalData: {
    userInfo: null,
    isConnected: true
  }
});
