// components/echarts/index.js
import * as echarts from "../ec-canvas/echarts";

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    chartId: {
      type: String,
      value: "echarts-id"
    },
    canvasId: {
      type: String,
      value: "echarts-canvas-id"
    },
    option: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    ec: {
      lazyLoad: true
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    initChart() {
      console.log(1111);
      console.log(this.properties.chartId);
      this.chartComp = this.selectComponent("#" + this.properties.chartId);
      this.chartComp &&
        this.chartComp.init((canvas, width, height, dpr) => {
          const chart = echarts.init(canvas, null, {
            width: width,
            height: height,
            devicePixelRatio: dpr
          });
          chart.setOption(this.properties.option);
          this.chart = chart;
          return chart;
        });
    }
  },
  lifetimes: {
    attached() {
      this.initChart();
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
      if (this.chart) {
        this.chart.dispose();
        this.chart = null;
      }
    }
  },
  observers: {
    option: function (value) {
      if (this.chart) {
        this.chart.setOption(value);
      }
    }
  }
});
