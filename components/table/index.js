Component({
  /**
   * 外部样式类
   */
  externalClasses: ['header-row-class-name', 'row-class-name', 'cell-class-name', 'tfooter-row-class-name'],

  /**
   * 组件样式隔离
   */
  options: {
    styleIsolation: "isolated",
    multipleSlots: true, // 支持多个slot
  },

  /**
   * 组件的属性列表
   */
  properties: {
    // 配置header key的名字
    props: {
      type: Object,
      value: {
        label: 'label',
        prop: 'prop'
      }
    },
    // 是否动态表头
    isDynamicHeader: {
      type: Boolean,
      value: false
    },
    data: {
      type: Array,
      value: []
    },
    header: {
      type: Array,
      value: []
    },
    footer: {
      type: Array,
      value: []
    },
    // table的高度, 溢出可滚动
    height: {
      type: String,
      value: 'auto'
    },
    width: {
      type: Number || String,
      value: '100%'
    },
    // 单元格的宽度
    tdWidth: {
      type: Number,
      value: 35
    },
    // 固定表头 thead达到Header的位置时就应该被fixed了
    offsetTop: {
      type: Number,
      value: 150
    },
    // 是否带有纵向边框
    stripe: {
      type: Boolean,
      value: false
    },
    // 是否带有纵向边框
    border: {
      type: Boolean,
      value: false
    },
    msg: {
      type: String,
      value: '暂无数据~'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    scrolWidth: '100%'
  },

  /**
   * 组件的监听属性
   */
  observers: {
    // 在 numberA 或者 numberB 被设置时，执行这个函数
    'header': function (header) {
      const reducer = (accumulator, currentValue) => {
        return accumulator + Number(currentValue.width || 150)
      };
      const scrolWidth = header.reduce(reducer, 0)
      this.setData({
        scrolWidth: scrolWidth
      })
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onRowClick(e) {
      this.triggerEvent('rowClick', e, e.currentTarget.dataset.it)
    },
  }
})