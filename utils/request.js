import { baseUrl } from "./config";

import { loginOut } from "./auth";

const request = options => {
  return new Promise((resolve, reject) => {
    const { data, method, url, header } = options;
    if (data && method !== "get") {
      options.data = JSON.stringify(data);
    }
    const exp = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
    const isFull = exp.test(url);
    options.url = isFull ? url : baseUrl + url;

    wx.request({
      ...options,
      header: {
        "Content-Type": "application/json",
        Authorization: wx.getStorageSync("token"),
        ...header
      },
      success: function (res) {
        if (res.data.status === 1) {
          resolve(res.data);
        } else if (res.data.status === 401) {
          loginOut();
          wx.reLaunch({
            url: "/pages/main/login/index"
          });
        } else {
          wx.showToast({
            title: (res.data && res.data.msg) || res.errMsg || "status!==1",
            icon: "none",
            duration: 5000
          });
          reject(res.data);
        }
      },
      fail: function (res) {
        wx.showToast({
          title: (res.data && res.data.msg) || res.errMsg || "接口错误",
          icon: "none",
          duration: 5000
        });
        reject(res.data);
      }
    });
  });
};
export default request;
