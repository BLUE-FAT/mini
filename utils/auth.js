import { login as getLogin, register as getRegister } from "../api/user";

export async function checkSession() {
  return new Promise((resolve, reject) => {
    wx.checkSession({
      success() {
        return resolve(true);
      },
      fail() {
        return resolve(false);
      }
    });
  });
}

export async function getWxCode() {
  return new Promise((resolve, reject) => {
    wx.login({
      success(res) {
        return resolve(res.code);
      },
      fail() {
        wx.showToast({
          title: "获取code失败",
          icon: "none"
        });
        return resolve("获取code失败");
      }
    });
  });
}

export async function checkHasLogined() {
  const token = wx.getStorageSync("token");
  if (!token) {
    return false;
  }
  const loggined = await checkSession();
  if (!loggined) {
    wx.removeStorageSync("token");
    return false;
  }
  return true;
}

export async function getUserInfo() {
  return new Promise((resolve, reject) => {
    wx.getUserInfo({
      success: res => {
        return resolve(res);
      },
      fail: err => {
        console.error(err);
        return resolve();
      }
    });
  });
}

export async function checkAndAuthorize(scope) {
  return new Promise((resolve, reject) => {
    wx.getSetting({
      success(res) {
        if (!res.authSetting[scope]) {
          wx.authorize({
            scope: scope,
            success() {
              resolve();
            },
            fail(e) {
              console.error(e);
              // if (e.errMsg.indexof("auth deny") != -1) {
              //   wx.showToast({
              //     title: e.errMsg,
              //     icon: "none"
              //   });
              // }
              wx.showModal({
                title: "无权操作",
                content: "需要获得您的授权",
                showCancel: false,
                confirmText: "立即授权",
                confirmColor: "#e64340",
                success(res) {
                  wx.openSetting();
                },
                fail(e) {
                  console.error(e);
                  reject(e);
                }
              });
            }
          });
        } else {
          resolve();
        }
      },
      fail(e) {
        console.error(e);
        reject(e);
      }
    });
  });
}

export async function usernameLogin(data) {
  return getLogin(data).then(res => {
    wx.setStorageSync("token", res.token);
    wx.setStorageSync("roles", JSON.stringify(res.user.roleKeys));
  });
}

export async function login(page) {
  const _this = this;
  wx.login({
    success: function (res) {
      getLogin(res.code).then(function (res) {
        if (res.code === 401) {
          // 去注册
          _this.register(page);
          return;
        }
        if (res.code !== 0) {
          // 登录错误
          wx.showModal({
            title: "无法登录",
            content: res.msg,
            showCancel: false
          });
          return;
        }
        wx.setStorageSync("token", res.token);
        wx.setStorageSync("roles", JSON.stringify(res.user.roleKeys));
        if (page) {
          page.onShow();
        }
      });
    }
  });
}

export async function register(page) {
  let _this = this;
  wx.login({
    success: function (res) {
      let code = res.code;
      wx.getUserInfo({
        success: function (res) {
          let iv = res.iv;
          let encryptedData = res.encryptedData;
          getRegister({
            code: code,
            encryptedData: encryptedData,
            iv: iv
          }).then(function (res) {
            _this.login(page);
          });
        }
      });
    }
  });
}

export function loginOut() {
  wx.removeStorageSync("token");
  wx.removeStorageSync("roles");
}
